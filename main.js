const fs = require("fs");
const readline = require("readline");

const util = require("./lib/util.js");
const solver = require("./lib/solver.js");
const unit = require("./lib/unit.js");

function _main(args) {	
	
	const usage = `Usage: 
	- node main.js /file/path.ext
	- node main.js --test`;
	
	// must have file path as argument
	if (args.length < 2 || args.length > 3) {
		console.log("Incorrect number of arguents provided.");
		console.log(usage);
		process.exit();
	}

	if (args.length == 3 && args[2] === "--test") {
		// unit test format validation	
		unit.assert("grid format valid:", util.validateFormat("5x5", /\dx\d/) == true, true);
		unit.assert("grid format invalid: has no x", util.validateFormat("54", /\dx\d/) == true, false);
		unit.assert("grid format invalid: has non-digits", util.validateFormat("5xY", /\dx\d/) == true, false);
		
		unit.assert("grid line valid: ", util.validateFormat("A B N J H", /^[A-Za-z\s ]+[A-Za-z]$/) == true, true);
		unit.assert("grid line invalid: has digits", util.validateFormat("A B 9 J H", /^[A-Za-z\s ]+[A-Za-z]$/) == true, false);
		unit.assert("grid line invalid: has special", util.validateFormat("A % N J H", /^[A-Za-z\s ]+[A-Za-z]$/) == true, false);
		unit.assert("grid line invalid: has end space", util.validateFormat("A J N J H ", /^[A-Za-z\s ]+[A-Za-z]$/) == true, false);
		
		unit.assert("grid word valid: ", util.validateFormat("ALpHA", /^[A-Za-z\s ]+[A-Za-z]$/) == true, true);
		unit.assert("grid word invalid: has digits", util.validateFormat("A9PHA", /^[A-Za-z\s ]+[A-Za-z]$/) == true, false);
		unit.assert("grid word invalid: has special", util.validateFormat("ALP$A", /^[A-Za-z\s ]+[A-Za-z]$/) == true, false);
		unit.assert("grid word invalid: has end space", util.validateFormat("ALPHA ", /^[A-Za-z\s ]+[A-Za-z]$/) == true, false);

		// unit test distance calculation
		var p1 = {"row": 3, "column": 2};
		var pv = {"row": 2, "column": 2};
		var ph = {"row": 3, "column": 3};
		var pd = {"row": 2, "column": 3};
		var pv2 = {"row": 0, "column": 2};
		var ph2 = {"row": 3, "column": 0};
		var pd2 = {"row": 0, "column": 0};

		unit.assert("distance one step vertical: ", util.getDistance(p1, pv) == 1, true);
		unit.assert("distance one step horizontal: ", util.getDistance(p1, ph) == 1, true);
		unit.assert("distance one step diagonal: ", util.getDistance(p1, pd) == 1, true);
		
		unit.assert("distance three step vertical: ", util.getDistance(p1, pv2) == 3, true);
		unit.assert("distance two step horizontal: ", util.getDistance(p1, ph2) == 2, true);
		unit.assert("distance offset diagonal: ", util.getDistance(p1, pd2) == 3, true);

		// unit test direction calculations
		unit.assert("direction one step north", util.getDirection(p1, pv) == "N", true);
		unit.assert("direction one step east", util.getDirection(p1, ph) == "E", true);
		unit.assert("direction one step north east", util.getDirection(p1, pd) == "NE", true);
		
		unit.assert("direction three step north", util.getDirection(p1, pv2) == "N", true);
		unit.assert("direction two step west", util.getDirection(p1, ph2) == "W", true);
		unit.assert("direction offset north west", util.getDirection(p1, pd2) == "NW", true);

		// unit test solver functions
		var lpositions = {
			"A": [{"row": 0, "column": 1},{"row": 2, "column": 2}],
			"B": [{"row": 0, "column": 0},{"row": 0, "column": 2}],
			"C": [{"row": 0, "column": 3},{"row": 3, "column": 3},{"row": 4, "column": 0}],
			"D": [{"row": 0, "column": 4}],
			"E": [{"row": 1, "column": 0}],
			"F": [{"row": 1, "column": 4}],
			"G": [{"row": 2, "column": 0},{"row": 4, "column": 2}],
			"H": [{"row": 2, "column": 1}],
			"I": [{"row": 2, "column": 3}],
			"J": [{"row": 2, "column": 4}],
			"K": [{"row": 3, "column": 0},{"row": 4, "column": 4}],
			"L": [{"row": 1, "column": 1},{"row": 3, "column": 4}],
			"M": [{"row": 4, "column": 1},{"row": 4, "column": 3}],
			"N": [{"row": 1, "column": 3}],
			"R": [{"row": 3, "column": 3}],
			"Y": [{"row": 1, "column": 2},{"row": 3, "column": 1}],
		};

		solver.load(lpositions, []);

		var wd = solver.find("BLACK");
		unit.assert("solver find word diagonal: ", wd.word === "BLACK", true);
		unit.assert("solver find word diagonal start: ", (wd.start.row == 0 && wd.start.column == 0), true);
		unit.assert("solver find word diagonal end: ", (wd.end.row == 4 && wd.end.column == 4), true);
		
	
		var wn = solver.find("GAB");
		unit.assert("solver find word not in grid: ", wn == null, true);
		
		// return to prevent the rest from running
		// and causing errors
		return;
	}

	/**
	 * check if file path exists
	 *
	 * I have been having trouble trying to find a way to
	 * catch the error if the file doesn't exist.
	 *
	 * if an invalid file path is provided, the program
	 * will throw the error and crash.
	 */
	if (args.length == 3) {

		var stats = fs.statSync(args[2]);

		if (!stats.isFile()) {
			console.log("Invalid file path given.");
			console.log(usage);
			process.exit();
		}
	}

	var list_positions = {};
	var list_words = [];
	
	var grid_rows = 0;
	var grid_columns = 0;

	var file_index = 0;
	var file_column = 0;
	
	const file = args[2];
	const file_data = fs.readFileSync(file, "utf-8");
	const file_lines = file_data.split(/\r?\n/);
	
	/**
	 * instead of searching the entire grid for every character of a word
	 * thie following code will build an object that can be indexed using
	 * a character A-Z
	 *
	 * each index has a list of positions for where the character is in
	 * the grid.
	 *
	 * instead of searching the entire grid and the eight cells around each
	 * character, this will allow indexing directly ot the character needed.
	 */
	file_lines.splice(file_lines.length - 1, 1);
	file_lines.forEach((line) => {

		// check first line for grid format
		if (file_index == 0) {
			if (!util.validateFormat(line, /\dx\d/)) {
				console.log("ERROR: file: " + file + " line: " + (file_index + 1) + ", incorrect grid format. Grid format must be specified as #x#.");
				process.exit();
			}

			var x = line.indexOf("x");
			grid_rows = parseInt(line.substring(0, x));
			grid_columns = parseInt(line.substring(x+1, line.length));
		}

 		// check next lines for grid lines/characters
		if (file_index > 0 && file_index-1 < grid_rows) {
			if (!util.validateFormat(line, /^[A-Za-z\s ]+[A-Za-z]$/)) {
				console.log("ERROR: file: " + file + " line: " + (file_index + 1) + ", incorrect line format. Grid line may only contain letters and spaces.");
				process.exit();
			}

			var line_stripped = line.replace(/\s/g, "");
			if (line_stripped.length > grid_columns) {
				console.log("ERROR: file: " + file + " line: " + (file_index + 1) + ", incorrect line format. Grid line may only have " + grid_columns + " characters.");
				process.exit();
			}

			while (file_column < grid_columns) {
					
				var character = line_stripped.charAt(file_column);
				var character_ascii = line_stripped.charCodeAt(file_column) - 65;
	
				if (!list_positions.hasOwnProperty(character)) {
					list_positions[character] = [];
				}

				list_positions[character].push({
					"row": file_index - 1,
					"column": file_column
				});

				file_column ++;
			}

			file_column = 0;
		}

		// check next lines for words to search
		if (file_index > grid_rows) {
			if (!util.validateFormat(line, /^[A-Za-z\s ]+[A-Za-z]$/)) {
				console.log("ERROR: file: " + file + " line: " + (file_index + 1) + ", incorrect word format. " + line + " contains invalid characters.");
				process.exit();
			}
			if (line.length > grid_rows + 1 || line.length > grid_columns + 1) {	
				console.log("ERROR: file: " + file + " line: " + (file_index + 1) + ", incorrect word format. " + line + " is too long.");
				process.exit();
			}

			list_words.push(line);
		}

		file_index ++;
	});

	solver.load(list_positions, list_words);
	solver.solve();
}

_main(process.argv);
