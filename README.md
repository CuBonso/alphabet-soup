# Alphabet Soup
> This program will take a file containing a grid of characters, a list of words to search for and return the starting and ending positions of each word.

## Files Included
Main:
* __main.js__: the main program

Library
* __lib/unit.js__: library file containing the unit module and test functions
* __lib/util.js__: library file containing the util module and utility functions
* __lib/solver.js__: library file containing the solver module and functions

Test
* __test_3x3_basic.txt__
* __test_5x5_advanced.txt__
* __test_5x5_bad_grid_format.txt__
* __test_5x5_bad_grid_line.txt__
* __test_5x5_bad_grid_line_digit.txt__
* __test_5x5_bad_grid_word.txt__
* __test_5x5_bad_grid_word_digit.txt__
* __test_10x10_mix.txt__
* __test_10x10_diagnals.txt__
* __test_10x10_verticals.txt__
* __test_10x10_horizontals.txt__

## How to run
* Run with NodeJS
* Navigate to the root project directory.

Run using the following command. Test files can be found in the /test directory
```
node main.js path/to/test/file
```

Run using the following command to run unit testing.
```
node main.js --test
```

## Requirements
NodeJS v16+

## Issues
* There is currently a problem not being able to catch the file not found error when an invalid filepath is provided as an argument. Several attempts with different methods failed.
* There is currently a problem when searching for a word that may not exist in the board. If the character doesn't exist on the board there may be a crash, though sometimes not. The root cause has not been pinned down as of yet.
