var Util = (function() {
	
	/**
	 * _format
	 *
	 * this function will validate a value to
	 * a specific regex pattern.
	 *
	 * @param value		- the value to test
	 * @param pattern	- the pattern to match
	 *
	 * @return			- boolean pattern result
	 */
	function _format(value, pattern) {
		return pattern.test(value);
	}
	
	/**
	 * _getDistance
	 *
	 * this function will calculate the distance between
	 * the row and column grid positions and return the
	 * higher of the two,
	 *
	 * @param from		- the start position
	 * @param to		- the end position
	 *
	 * @return			- int distance
	 */
	function _getDistance(from, to) {
		var distance_row = 0;
		var distance_column = 0;

		if (from.row > to.row) {
			distance_row = from.row - to.row;
		} else {
			distance_row = to.row - from.row;
		}

		if (from.column > to.column) {
			distance_column = from.column - to.column;
		} else {
			distance_column = to.column - from.column;
		}

		return Math.max(distance_row, distance_column);
	}

	/**
	 * _getDirection
	 *
	 * this function will build a direction string based on
	 * the value of the rows and columns of two positions.
	 *
	 * @param from		- the start position
	 * @param to		- the end position
	 *
	 * @return			- string direction as compass values
	 */
	function _getDirection(from, to) {
		var direction = "";

		if (from.row > to.row) {
			direction += "N";
		} else if (from.row < to.row) {
			direction += "S";
		}

		if (from.column > to.column) {
			direction += "W";
		} else if (from.column < to.column) {
			direction += "E";
		}

		return direction;
	}

	/**
	 * public function
	 */
	return {
		validateFormat:	_format,
		getDistance:	_getDistance,
		getDirection:	_getDirection
	};
})();

module.exports = Util;
