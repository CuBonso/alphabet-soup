var Unit = (function() {

	var units_passed = 0;
	var units_failed = 0;
	var units_total = 0;

	/**
	 * _assert
	 *
	 * this function will act as an assertion checker
	 * amd print the results
	 *
	 * @param name		- the test name
	 * @param assertion	- the assertion expression (as a boolean)
	 * #param expect	- the expected result
	 */
	function _assert(name, assertion, expected) {
		var unit_result = "";

		if (assertion == expected) {
			units_passed ++;
			unit_result = "passed";
		} else {
			units_failed ++;
			unit_result = "failed";
		}
		units_total ++;

		console.log("test[ " + units_total + " ] " + name);
		console.log("- status= " + unit_result + " assertion= " + assertion + " expected= " + expected);
		console.log("");
	}

	return {
		assert: _assert
	};
})();

module.exports = Unit;
